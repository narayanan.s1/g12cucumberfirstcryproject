package CalledMethods;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

public class PaymentPage {

	WebDriver driver;
	//Actions act = new Actions(driver);
	public PaymentPage(WebDriver driver)
	{
		this.driver=driver;
		}
	@FindBy(xpath="//label[text()='place Order']")
	private WebElement placeorderbtn;
	public void placeord()
	{
		//JavascriptExecutor js = (JavascriptExecutor) driver;

		//js.executeScript("arguments[0].click()",addnamebtn);
	   placeorderbtn.click();
	    
	}
	@FindBy(xpath="//label[text()='Credit/Debit cards']")
	private WebElement creditcardbtn;
	public void cardbtn()
	{
		creditcardbtn.click();
	}
	@FindBy(id="vpc_CardNum")
	private WebElement cardnumbertxt;
	public void cardnotxt()
	{
		cardnumbertxt.sendKeys("0987654321098765");
	}
	@FindBy(id="txtNameOnCard")
	private WebElement cardnametxt;
	public void cardnamtxt()
	{
		cardnametxt.sendKeys("Narayanan S");
	}
	@FindBy(id="CardExpiryMonth")
	private WebElement cardmonth;
	public void cardmon()
	{
		Select s=new Select(cardmonth);
		s.selectByVisibleText("06");	

	}
	@FindBy(id="CardExpiryYear")
	private WebElement cardyear;
	public void cardyr()
	{
		Select s=new Select(cardyear);
		s.selectByVisibleText("23");	

	}
	@FindBy(xpath="//input[@placeholder='CVV']")
	private WebElement cardcvvp;
	public void cardcvv()
	{
		cardcvvp.sendKeys("032");
	}
	@FindBy(xpath="//div[@id='BtnCardPayNow']")
	private WebElement cardpaynowbtn;
	public void cardpaybtn()
	{
		cardpaynowbtn.click();
	}
	

	
	
	
	

	
	
	
}
