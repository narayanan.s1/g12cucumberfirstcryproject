package CalledMethods;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.devtools.idealized.Javascript;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

public class Searchpage {

	WebDriver driver;
    //Actions act = new Actions(driver);
    public Searchpage(WebDriver driver)
    {
        this.driver=driver;
}
    @FindBy(xpath="//a[text()=' Toys']")
    private WebElement Toys;
    public void toyscatag()
    {
        Actions act = new Actions(driver);
    	act.moveToElement(Toys).build().perform();
    
    }
    @FindBy(linkText="Blocks & Construction Sets")
    private WebElement blockstoy;
    public void blocktoytag()
    {
        Actions act = new Actions(driver);
        act.doubleClick(blockstoy).perform();
        
    }
    @FindBy(xpath="//a[text()='Pine Kids Junior Stacking & Tumbling Tower - 39 Blocks']")
    private WebElement buildingblocks;
    public void builblocks()
    {
    	JavascriptExecutor js = (JavascriptExecutor) driver;
    	js.executeScript("arguments[0].scrollIntoView()", buildingblocks);
     	buildingblocks.click();
        
    }
    
    
    
}
