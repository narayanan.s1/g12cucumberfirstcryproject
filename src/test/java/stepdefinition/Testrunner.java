package stepdefinition;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

public class Testrunner {
	@CucumberOptions(tags = "", features = "src/test/resources/Feature/scenerio.feature",
			glue = "stepdefinition",
			plugin = { "pretty", "html:target/cucumber-reportss" },
			monochrome = true)
			public class TestRunner extends AbstractTestNGCucumberTests{
				
			}

}
