package stepdefinition;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;

import CalledMethods.AddToCart;
import CalledMethods.AddressPage;
import CalledMethods.LoginPage;
import CalledMethods.PaymentPage;
import CalledMethods.Searchpage;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class Firstcryapplication {
	static WebDriver driver;
	@Given("User is on Home Page of Firstcry aplication")
	public void user_is_on_home_page_of_firstcry_aplication() {
	WebDriver driver = new ChromeDriver();	
	driver.get("https://www.firstcry.com/");	
	driver.manage().window().maximize();
	driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
	}

	@When("User click on sign button")
	public void user_click_on_sign_button() throws InterruptedException {
    LoginPage log = PageFactory.initElements(driver, LoginPage.class);
    Thread.sleep(6000);
    log.loginbtn();

	}
	

	@When("User enters email and click continue")
	public void user_enters_email_and_click_continue() throws InterruptedException {
	 LoginPage log = PageFactory.initElements(driver, LoginPage.class);
	 Thread.sleep(6000);
     log.logmailbtn();
	 log.logconbtn();
	}

	@When("User have to enter valid OTP and click on startshoping button")
	public void user_have_to_enter_valid_otp_and_click_on_startshoping_button() {
	}

	@Then("Message displayed Login Successfully")
	public void message_displayed_login_successfully() {
		System.out.println("loginsuccessfully");
	}

	@Given("user have  able to  select the Toys Section")
	public void user_have_able_to_select_the_toys_section() {
        Searchpage search = PageFactory.initElements(driver, Searchpage.class);
        search.toyscatag();

	}

	@When("user have to select Blocks category")
	public void user_have_to_select_blocks_category() {
    Searchpage search = PageFactory.initElements(driver, Searchpage.class);
    //search.blocktoytag();
	}

	@Then("user have to click Buildinglocks")
	public void user_have_to_click_buildinglocks() {
	    Searchpage search = PageFactory.initElements(driver, Searchpage.class);
        //search.builblocks();
	}

	@Given("user have to click Add to cart")
	public void user_have_to_click_add_to_cart() throws InterruptedException {
        AddToCart atc = PageFactory.initElements(driver, AddToCart.class);
       // atc.addtocart();

	}

	@Then("click Go To cart button")
	public void click_go_to_cart_button() {
	}

	@Given("user have to click Addadress button")
	public void user_have_to_click_addadress_button() {
        AddressPage add = PageFactory.initElements(driver, AddressPage.class);
        //add.addbtn();

	}

	@When("user have to enter full name")
	public void user_have_to_enter_full_name() {
        AddressPage add = PageFactory.initElements(driver, AddressPage.class);
       // add.addnamtxt();

	}

	@When("user have to enter mobile number")
	public void user_have_to_enter_mobile_number() {
		AddressPage add = PageFactory.initElements(driver, AddressPage.class);
        //add.addmobtxt();

		}

	@When("user have to enter postalcode")
	public void user_have_to_enter_postalcode() {
		AddressPage add = PageFactory.initElements(driver, AddressPage.class);
        //add.addpinctxt();

		
	}

	@When("user have to enter address {int}")
	public void user_have_to_enter_address(Integer int1) {
		AddressPage add = PageFactory.initElements(driver, AddressPage.class);
		//add.addadd1txt();
        //add.addadd2txt();
	}

	@When("user have to enter landmark")
	public void user_have_to_enter_landmark() {
		AddressPage add = PageFactory.initElements(driver, AddressPage.class);
        //add.addadd3txt();

	}

	@Then("user have to click save button")
	public void user_have_to_click_save_button() {
		AddressPage add = PageFactory.initElements(driver, AddressPage.class);
		//add.addsavadd();
	}

	@Given("user have to click placeorder button")
	public void user_have_to_click_placeorder_button() {
        PaymentPage pay = PageFactory.initElements(driver,PaymentPage.class);
       // pay.placeord();

	}

	@When("user have to select paymentment method")
	public void user_have_to_select_paymentment_method() {
        PaymentPage pay = PageFactory.initElements(driver,PaymentPage.class);
       // pay.cardbtn();

	}

	@When("user have to enter valid card details")
	public void user_have_to_enter_valid_card_details() {
        PaymentPage pay = PageFactory.initElements(driver,PaymentPage.class);
        //pay.cardnotxt();
       // pay.cardnamtxt();
        //pay.cardmon();
        //pay.cardyr();
        //pay.cardcvv();

	}

	@Then("user have to click pay button")
	public void user_have_to_click_pay_button() {
        PaymentPage pay = PageFactory.initElements(driver,PaymentPage.class);
        //pay.cardpaybtn();


	}

	@Then("user have to get order details")
	public void user_have_to_get_order_details() {
        PaymentPage pay = PageFactory.initElements(driver,PaymentPage.class);

	}

	@When("User LogOut from the Application")
	public void user_log_out_from_the_application() {
	}

	@Then("Message displayed LogOut SuccessfullFeature: Firstcry Application functionality")
	public void message_displayed_log_out_successfull_feature_firstcry_application_functionality() {
	}

}
