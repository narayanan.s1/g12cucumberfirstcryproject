Feature: Fristcry Application functionality
  
  Scenario: Successful Login with Valid Credentials
    Given User is on Home Page of Firstcry aplication
    When User click on sign button
    And User enters email and click continue
    And User have to enter valid OTP and click on startshoping button
    Then Message displayed Login Successfully
  
  Scenario: To validate search for product functionality
    Given user have  able to  select the Toys Section
    When user have to select Blocks category
    Then user have to click Buildinglocks
  
  Scenario: To validate Addtocart functionality
    Given user have to click Add to cart
    Then click Go To cart button
 
  Scenario: To validate AddAddress functionality
    Given user have to click Addadress button
    When user have to enter full name
    And user have to enter mobile number
    And user have to enter postalcode 
    And user have to enter address 1
    And user have to enter address 2
    And user have to enter landmark
    Then user have to click save button
 
  Scenario: To validate payment functionality
    Given user have to click placeorder button
    When user have to select paymentment method
    When user have to enter valid card details
    Then user have to click pay button
    And user have to click pay button
    And user have to get order details
  
  Scenario: Successful LogOut
    When User LogOut from the Application
    Then Message displayed LogOut SuccessfullFeature: Firstcry Application functionality












